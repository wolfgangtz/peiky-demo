FROM python:3.6.5-jessie

WORKDIR /
COPY . /app

WORKDIR /app

RUN pip install --no-cache-dir -r requirements.txt

EXPOSE 8000

CMD ["sh", "start.sh"]