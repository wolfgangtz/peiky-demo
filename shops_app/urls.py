from django.urls import path
from django.urls import re_path
from shops_app.views import shop
from shops_app.views import product



urlpatterns = [
    # Shop
    path(
        '',
        shop.ShopList.as_view(),
        name=shop.ShopList.name
    ),
    re_path(
        '^(?P<pk>[0-9]+)/$',
        shop.ShopDetail.as_view(),
        name=shop.ShopDetail.name
    ),


    # Product
    path(
        'products/',
        product.ProductList.as_view(),
        name=product.ProductList.name
    ),
    re_path(
        '^products/(?P<pk>[0-9]+)/$',
        product.ProductDetail.as_view(),
        name=product.ProductDetail.name
    ),
]


