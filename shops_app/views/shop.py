from rest_framework import generics
from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAuthenticated
from shops_app.models.shop import Shop
from shops_app.serializers.shop import ShopSerializer
from shops_app.serializers.shop import ShopProductsSerializer

@permission_classes((IsAuthenticated, ))
class ShopList(generics.ListCreateAPIView):
    queryset = Shop.objects.all()
    serializer_class = ShopSerializer
    name = 'shop-list'

@permission_classes((IsAuthenticated, ))
class ShopDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Shop.objects.all()
    serializer_class = ShopProductsSerializer
    name = 'shop-detail'

    def put(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)