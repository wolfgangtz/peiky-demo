from rest_framework import generics
from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAuthenticated
from shops_app.models.product import Product
from shops_app.serializers.product import ProductSerializer
from shops_app.serializers.product import ProductSalesSerializer

@permission_classes((IsAuthenticated, ))
class ProductList(generics.ListCreateAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    name = 'product-list'

@permission_classes((IsAuthenticated, ))
class ProductDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductSalesSerializer
    name = 'product-detail'

    def put(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)