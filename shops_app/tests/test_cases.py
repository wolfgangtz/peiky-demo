from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APIClient
from rest_framework import status
from shops_app.models.product import Product
from sales_app.models.sale import Sale
from sales_app.helpers.helpers_function import create_temporary_user
from sales_app.helpers.helpers_function import create_temporary_shop
from sales_app.helpers.helpers_function import create_temporary_product


class TestCase(TestCase):

    def setUp(self):
        self.client = APIClient()

    def test_cannot_create_product_negative_stock_value(self):
        url = reverse('product-list')
        self.shop = create_temporary_shop()
        self.token, _ = create_temporary_user()
        data = {
            'shop': self.shop.id,
            'name': 'Test Product',
            'stock': -10,
        }

        self.client.credentials(HTTP_AUTHORIZATION='Token ' +self.token.key)
        self.response = self.client.post(url, data, format='json')
        self.assertEqual(self.response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Product.objects.count(), 0)


    def test_cannot_sold_a_product_that_have_not_stock(self):
        url = reverse('sale-list')
        self.product = create_temporary_product()
        self.token, self.user = create_temporary_user()
        data = {
            'product': self.product.id,
            'amount_items_sold': 150,
            'sale_agent': self.user.id,
        }

        self.client.credentials(HTTP_AUTHORIZATION='Token ' +self.token.key)
        self.response = self.client.post(url, data, format='json')
        self.assertEqual(self.response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Sale.objects.count(), 0)


