from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APIClient
from rest_framework import status
from shops_app.models.product import Product
from sales_app.helpers.helpers_function import create_temporary_user
from sales_app.helpers.helpers_function import create_temporary_shop
from sales_app.helpers.helpers_function import create_temporary_product


class ProductAPITestCase(TestCase):

    def setUp(self):
        self.client = APIClient()

    def test_post_product_is_forbidden_if_user_is_not_authenticated(self):
        url = reverse('product-list')
        self.shop = create_temporary_shop()
        data = {
            'shop': self.shop.id,
            'name': 'Test Product',
            'stock': 100,
        }
        self.response = self.client.post(url, data, format='json')
        self.assertEqual(self.response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(Product.objects.count(), 0)

    def test_post_product_creates_objects_if_user_is_authenticated(self):
        url = reverse('product-list')
        self.shop = create_temporary_shop()
        self.token, _ = create_temporary_user()
        data = {
            'shop': self.shop.id,
            'name': 'Test Product',
            'stock': 100,
        }

        self.client.credentials(HTTP_AUTHORIZATION='Token ' +self.token.key)
        self.response = self.client.post(url, data, format='json')
        self.assertEqual(self.response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Product.objects.count(), 1)


    def test_get_collection_product_is_forbidden_if_user_is_not_authenticated(self):
        url = reverse('product-list')
        self.response = self.client.get(url, format='json')
        self.assertEqual(self.response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(Product.objects.count(), 0)

    def test_get_collection_product_returns_objects_if_user_is_authenticated(self):
        url = reverse('product-list')
        self.token, _ = create_temporary_user()
        create_temporary_product()

        self.client.credentials(HTTP_AUTHORIZATION='Token ' +self.token.key)
        self.response = self.client.get(url, format='json')
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)
        self.assertEqual(Product.objects.count(), 1)

    def test_get_object_product_is_forbidden_if_user_is_not_authenticated(self):
        url = reverse('product-detail', kwargs = { 'pk': 1 })
        self.response = self.client.get(url, format='json')
        self.assertEqual(self.response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(Product.objects.count(), 0)

    def test_get_object_product_returns_objects_if_user_is_authenticated(self):
        
        self.token, _ = create_temporary_user()
        self.product = create_temporary_product()
        url = reverse('product-detail', kwargs = { 'pk': self.product.id })

        self.client.credentials(HTTP_AUTHORIZATION='Token ' +self.token.key)
        self.response = self.client.get(url, format='json')
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)
        self.assertEqual(Product.objects.count(), 1)
        self.assertEqual(Product.objects.first().id, self.product.id)


    def test_update_product_is_forbidden_if_user_is_not_authenticated(self):
        url = reverse('product-detail', kwargs = { 'pk': 1 })

        data = {
            'shop': 1,
            'name': 'Test Product',
            'stock': 100,
        }
        self.response = self.client.put(url, data, format='json')
        self.assertEqual(self.response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(Product.objects.count(), 0)

    def test_update_product_updates_object_if_user_is_authenticated(self):
        
        self.token, _ = create_temporary_user()
        self.product = create_temporary_product()
        url = reverse('product-detail', kwargs = { 'pk': self.product.id })

        data = {
            'shop': self.product.shop.id,
            'name': 'Test Product',
            'stock': 50,
        }

        self.client.credentials(HTTP_AUTHORIZATION='Token ' +self.token.key)
        self.response = self.client.put(url, data, format='json')
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)
        self.assertEqual(Product.objects.count(), 1)
        self.assertEqual(Product.objects.first().stock, 50)


    def test_destroy_product_is_forbidden_if_user_is_not_authenticated(self):
        url = reverse('product-detail', kwargs = { 'pk': 1 })

        self.response = self.client.delete(url, format='json')
        self.assertEqual(self.response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(Product.objects.count(), 0)

    def test_destroy_product_destroys_object_if_user_is_authenticated(self):
        
        self.token, _ = create_temporary_user()
        self.product = create_temporary_product()
        url = reverse('product-detail', kwargs = { 'pk': self.product.id })

        self.client.credentials(HTTP_AUTHORIZATION='Token ' +self.token.key)
        self.response = self.client.delete(url, format='json')
        self.assertEqual(self.response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Product.objects.count(), 0)
