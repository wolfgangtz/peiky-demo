from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APIClient
from rest_framework import status
from shops_app.models.shop import Shop
from sales_app.helpers.helpers_function import create_temporary_user
from sales_app.helpers.helpers_function import create_temporary_shop


class ShopAPITestCase(TestCase):

    def setUp(self):
        self.client = APIClient()

    def test_post_shop_is_forbidden_if_user_is_not_authenticated(self):
        url = reverse('shop-list')
        data = {
            'address': 'Cll 179 # 6 - 64',
            'name': 'Test Shop',
            'phone': '3166178464',
        }
        self.response = self.client.post(url, data, format='json')
        self.assertEqual(self.response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(Shop.objects.count(), 0)

    def test_post_shop_creates_objects_if_user_is_authenticated(self):
        url = reverse('shop-list')
        self.token, _ = create_temporary_user()
        data = {
            'address': 'Cll 179 # 6 - 64',
            'name': 'Test Shop',
            'phone': '3166178464',
        }

        self.client.credentials(HTTP_AUTHORIZATION='Token ' +self.token.key)
        self.response = self.client.post(url, data, format='json')
        self.assertEqual(self.response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Shop.objects.count(), 1)


    def test_get_collection_shop_is_forbidden_if_user_is_not_authenticated(self):
        url = reverse('shop-list')
        self.response = self.client.get(url, format='json')
        self.assertEqual(self.response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(Shop.objects.count(), 0)

    def test_get_collection_shop_returns_objects_if_user_is_authenticated(self):
        url = reverse('shop-list')
        self.token, _ = create_temporary_user()
        create_temporary_shop()

        self.client.credentials(HTTP_AUTHORIZATION='Token ' +self.token.key)
        self.response = self.client.get(url, format='json')
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)
        self.assertEqual(Shop.objects.count(), 1)

    def test_get_object_shop_is_forbidden_if_user_is_not_authenticated(self):
        url = reverse('shop-detail', kwargs = { 'pk': 1 })
        self.response = self.client.get(url, format='json')
        self.assertEqual(self.response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(Shop.objects.count(), 0)

    def test_get_object_shop_returns_objects_if_user_is_authenticated(self):
        
        self.token, _ = create_temporary_user()
        self.shop = create_temporary_shop()
        url = reverse('shop-detail', kwargs = { 'pk': self.shop.id })

        self.client.credentials(HTTP_AUTHORIZATION='Token ' +self.token.key)
        self.response = self.client.get(url, format='json')
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)
        self.assertEqual(Shop.objects.count(), 1)
        self.assertEqual(Shop.objects.first().id, self.shop.id)


    def test_update_shop_is_forbidden_if_user_is_not_authenticated(self):
        url = reverse('shop-detail', kwargs = { 'pk': 1 })

        data = {
            'address': 'Cll 179 # 6 - 64',
            'name': 'Test Shop',
            'phone': '3022882815',
        }

        self.response = self.client.put(url, data, format='json')
        self.assertEqual(self.response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(Shop.objects.count(), 0)

    def test_update_shop_updates_object_if_user_is_authenticated(self):
        
        self.token, _ = create_temporary_user()
        self.shop = create_temporary_shop()
        url = reverse('shop-detail', kwargs = { 'pk': self.shop.id })

        data = {
            'address': 'Cll 179 # 6 - 64',
            'name': 'Test Shop',
            'phone': '3022882815',
        }

        self.client.credentials(HTTP_AUTHORIZATION='Token ' +self.token.key)
        self.response = self.client.put(url, data, format='json')
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)
        self.assertEqual(Shop.objects.count(), 1)
        self.assertEqual(Shop.objects.first().phone, '3022882815')


    def test_destroy_shop_is_forbidden_if_user_is_not_authenticated(self):
        url = reverse('shop-detail', kwargs = { 'pk': 1 })

        self.response = self.client.delete(url, format='json')
        self.assertEqual(self.response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(Shop.objects.count(), 0)

    def test_destroy_shop_destroys_object_if_user_is_authenticated(self):
        
        self.token, _ = create_temporary_user()
        self.shop = create_temporary_shop()
        url = reverse('shop-detail', kwargs = { 'pk': self.shop.id })

        self.client.credentials(HTTP_AUTHORIZATION='Token ' +self.token.key)
        self.response = self.client.delete(url, format='json')
        self.assertEqual(self.response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Shop.objects.count(), 0)
