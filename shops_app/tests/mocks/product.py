from shops_app.models.product import Product

STOCK_MOCK = 100
NAME_MOCK = 'peiky'

def mock_product(shop):

    product = Product(
        name=NAME_MOCK,
        stock=STOCK_MOCK,
        shop=shop
    )
    return product