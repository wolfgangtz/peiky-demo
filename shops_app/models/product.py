from django.db import models
from shops_app.models.shop import Shop
from django.core.validators import MinValueValidator
from simple_history.models import HistoricalRecords


class Product(models.Model):
    history = HistoricalRecords()
    name = models.CharField(
        max_length=100,
        null=False,
        blank=False
    )
    shop = models.ForeignKey(
        Shop,
        on_delete=models.CASCADE,
        related_name='products',
        null=False,
        blank=False
    )
    stock = models.IntegerField(
        default=0,
        validators=[
            MinValueValidator(0)
        ]
    )
    creation_date = models.DateField(
        auto_now_add=True
    )

    def __str__(self):
        return "Product: '" + self.name + \
                "' In Shop: '" + self.shop.name + \
                "'. In Stock " + str(self.stock)