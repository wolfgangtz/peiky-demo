from django.db import models
from simple_history.models import HistoricalRecords


class Shop(models.Model):
    history = HistoricalRecords()
    name = models.CharField(
        max_length=100,
        null=False,
        blank=False
    )
    address = models.CharField(
        max_length=100,
        null=False,
        blank=False
    )
    phone = models.CharField(
        max_length=100,
        null=False,
        blank=False
    )
    creation_date = models.DateField(
        auto_now_add=True
    )


    def __str__(self):
        return self.name