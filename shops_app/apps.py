from django.apps import AppConfig


class ShopsAppConfig(AppConfig):
    name = 'shops_app'
