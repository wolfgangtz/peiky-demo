from django.contrib import admin
from shops_app.models.product import Product
from shops_app.models.shop import Shop


class ProductAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'stock', 'creation_date')
    search_fields = ('name', )
    list_per_page = 25



class ShopAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'address', 'phone', 'creation_date')
    search_fields = ('name', )
    list_per_page = 25



admin.site.register(Product, ProductAdmin)
admin.site.register(Shop, ShopAdmin)