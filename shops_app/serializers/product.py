from rest_framework import serializers
from shops_app.models.product import Product
from sales_app.serializers.sale import SaleSerializer

class ProductSerializer(serializers.ModelSerializer):

    class Meta:
        model = Product
        fields = (
            'id',
            'name',
            'shop',
            'stock',
            'creation_date'
        )
        read_only_fields = (
            'id',
        )


class ProductSalesSerializer(serializers.ModelSerializer):

    sales = SaleSerializer(many=True)

    class Meta:
        model = Product
        fields = (
            'id',
            'name',
            'shop',
            'stock',
            'creation_date',
            'sales'
        )
        read_only_fields = (
            'id',
        )
