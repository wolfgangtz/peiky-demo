from rest_framework import serializers
from shops_app.models.shop import Shop
from shops_app.serializers.product import ProductSerializer


class ShopSerializer(serializers.ModelSerializer):

    class Meta:
        model = Shop
        fields = (
            'id',
            'name',
            'address',
            'phone',
            'creation_date'
        )
        read_only_fields = (
            'id',
        )


class ShopProductsSerializer(serializers.ModelSerializer):

    products = ProductSerializer(many=True)

    class Meta:
        model = Shop
        fields = (
            'id',
            'name',
            'address',
            'phone',
            'creation_date',
            'products'
        )
        read_only_fields = (
            'id',
        )
