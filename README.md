# RESUME

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

This is a resume about solution of the Peiky Test for the BACKEND DEVELOPER position. The test will be developed with:
-	Virtualenv 15.1.0
-	Python 3.6.4
-	Django 2.2.3
-	DjangoRestFramework 3.9.4
-	Django_rest_swagger 2.2.0
-	Django_simple_history 2.7.2
-	Djang_rest_auth 0.9.5

# The Unitary Test
Please, run the Test before launch project, type:
```sh
$ python manage.py test
```
# INSTRUCTIONS TO RUN THE PROJECT - MANUALLY

Please follow the next steps in order to run the project manually and validate the functionality:

1.	Create a virtualenv, for more information please visit the next link: https://virtualenv.pypa.io/en/latest/. This tool is used to create an environment totally independent of you Laptop/PC environment, therefore, you will not have any trouble with the things installed inside the environment.
2.	Turn on the environment 

```sh
$ source venv/bin/activate
```

3.	Install the requirements: 

```sh
$ pip install -r requirements.txt
```

4.	A admin user is created when you run the migrations, then, please run:

```sh
$ python manage.py makemigrations
$ python manage.py migrate
```
the username: admin, password: admin, or you can create a user to login inside the Admin UI:
```sh
$ python manage.py createsuperuser
```

5.	Run the project:

```sh
$ python manage.py runserver
```

6.	Please type this URL in your browser: http://127.0.0.1:8000/admin/. Here, after login with the credentials created in the previous steps, you can admin all the shops, products and sales of the system (CAREFULL: this tool is only for admin’s because form here you can edit data directly to database).
7.	Please type this URL in your browser: http://127.0.0.1:8000/api/. Here, you can see and test the API of the backend, please keep on mind that you need to be login to see all the endpoints because you will need permissions to see this info.

# INSTRUCTIONS TO RUN THE PROJECT – DOCKERFILE

Please follow the next steps in order to run the project using dockerfile and validate the functionality:

1.	Build the Image:
```sh
$ docker build -t peikydemo/test .
```

2.	Validate if the image was created successfully type:
```sh
$ docker images
```

you should see one called peikydemo/test

3.	Run the project inside the image using this command:
```sh
$ docker run -p 8000:8000 -i -t peikydemo/test
```

4.	You can access to the same url’s above to verify the functionality

### Improvement Points

 - Write MORE Tests
 - Use another database more powerfull like Postgres
 - Use pagination for some endpoints
 - extend the user model of Django to link the users with shops and avoid editions in wrongs shops
 - Change to production mode
 - add logs

License
----

MIT


**Free Software, Hell Yeah!**

