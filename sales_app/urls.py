from django.urls import path
from django.urls import re_path
from sales_app.views import sale

urlpatterns = [
    # Sale
    path(
        'sales/',
        sale.SaleList.as_view(),
        name=sale.SaleList.name
    ),
    re_path(
        '^sales/(?P<pk>[0-9]+)/$',
        sale.SaleDetail.as_view(),
        name=sale.SaleDetail.name
    ),

]
