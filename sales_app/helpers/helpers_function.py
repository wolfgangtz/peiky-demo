from django.contrib.auth import get_user_model
from rest_framework.authtoken.models import Token
from sales_app.tests.mocks.sale import mock_sale
from shops_app.tests.mocks.product import mock_product
from shops_app.tests.mocks.shop import mock_shop

def create_temporary_user():
    User = get_user_model()
    user = User.objects.create_user(
        'temporary user',
        'temporary@peikydemo.com',
        'temporary'
    )
    token, _ = Token.objects.get_or_create(user=user)
    return token, user

def create_temporary_shop():
    shop = mock_shop()
    shop.save()

    return shop


def create_temporary_product():
    shop = mock_shop()
    shop.save()

    product = mock_product(
        shop=shop
    )
    product.save()

    return product

def create_temporary_sale(user):
    shop = mock_shop()
    shop.save()

    product = mock_product(
        shop=shop
    )
    product.save()

    sale = mock_sale(
        product=product,
        sale_agent=user
    )
    sale.save()
    return sale