from rest_framework import generics
from rest_framework import status
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAuthenticated
from sales_app.models.sale import Sale
from shops_app.models.product import Product
from sales_app.serializers.sale import SaleSerializer

@permission_classes((IsAuthenticated, ))
class SaleList(generics.ListCreateAPIView):
    queryset = Sale.objects.all()
    serializer_class = SaleSerializer
    name = 'sale-list'

    def create(self, request):
        # We can do this step from the serializer using validate function, but I prefer use
        # this metod bacause we can take the user from the request and avoid suplantation.
        # I override this metod for the cause above decripted and to add the validation
        # of the product stock.


        user = request.user
        product = get_object_or_404(Product, id=request.data["product"])
        amount_items_sold = request.data["amount_items_sold"]

        sale = {}
        sale['sale_agent'] = user.id
        sale['product'] = product.id
        sale['amount_items_sold'] = amount_items_sold

        sale_serializer = SaleSerializer(data=sale)

        if not sale_serializer.is_valid():
            return Response(
                sale_serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )


        if product.stock - amount_items_sold < 0:
            return Response(
                {'error': 'There are not this product available in stock'},
                status=status.HTTP_400_BAD_REQUEST
            )


        product.stock = product.stock - amount_items_sold
        product.save()
        sale_serializer.save()

        return Response(
            data=sale_serializer.data,
            status=status.HTTP_201_CREATED
        )


@permission_classes((IsAuthenticated, ))
class SaleDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Sale.objects.all()
    serializer_class = SaleSerializer
    name = 'sale-detail'

    def put(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)
