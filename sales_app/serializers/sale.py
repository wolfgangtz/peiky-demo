from rest_framework import serializers
from sales_app.models.sale import Sale


class SaleSerializer(serializers.ModelSerializer):

    class Meta:
        model = Sale
        fields = (
            'id',
            'product',
            'sale_date',
            'sale_agent',
            'amount_items_sold'
        )
        read_only_fields = (
            'id',
        )