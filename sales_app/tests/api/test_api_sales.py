from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APIClient
from rest_framework import status
from sales_app.models.sale import Sale
from sales_app.helpers.helpers_function import create_temporary_user
from sales_app.helpers.helpers_function import create_temporary_product
from sales_app.helpers.helpers_function import create_temporary_sale


class SalesAPITestCase(TestCase):

    def setUp(self):
        self.client = APIClient()

    def test_post_sales_is_forbidden_if_user_is_not_authenticated(self):
        url = reverse('sale-list')
        self.product = create_temporary_product()
        data = {
            'product': self.product.id,
            'amount_items_sold': 1,
            'sale_agent': 1,
        }
        self.response = self.client.post(url, data, format='json')
        self.assertEqual(self.response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(Sale.objects.count(), 0)

    def test_post_sales_creates_objects_if_user_is_authenticated(self):
        url = reverse('sale-list')
        self.product = create_temporary_product()
        self.token, self.user = create_temporary_user()
        data = {
            'product': self.product.id,
            'amount_items_sold': 1,
            'sale_agent': self.user.id,
        }

        self.client.credentials(HTTP_AUTHORIZATION='Token ' +self.token.key)
        self.response = self.client.post(url, data, format='json')
        self.assertEqual(self.response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Sale.objects.count(), 1)


    def test_get_collection_sales_is_forbidden_if_user_is_not_authenticated(self):
        url = reverse('sale-list')
        self.response = self.client.get(url, format='json')
        self.assertEqual(self.response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(Sale.objects.count(), 0)

    def test_get_collection_sales_returns_objects_if_user_is_authenticated(self):
        url = reverse('sale-list')
        self.token, self.user = create_temporary_user()
        create_temporary_sale(self.user)

        self.client.credentials(HTTP_AUTHORIZATION='Token ' +self.token.key)
        self.response = self.client.get(url, format='json')
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)
        self.assertEqual(Sale.objects.count(), 1)

    def test_get_object_sales_is_forbidden_if_user_is_not_authenticated(self):
        url = reverse('sale-detail', kwargs = { 'pk': 1 })
        self.response = self.client.get(url, format='json')
        self.assertEqual(self.response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(Sale.objects.count(), 0)

    def test_get_object_sales_returns_objects_if_user_is_authenticated(self):
        
        self.token, self.user = create_temporary_user()
        self.sale = create_temporary_sale(self.user)
        url = reverse('sale-detail', kwargs = { 'pk': self.sale.id })

        self.client.credentials(HTTP_AUTHORIZATION='Token ' +self.token.key)
        self.response = self.client.get(url, format='json')
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)
        self.assertEqual(Sale.objects.count(), 1)
        self.assertEqual(Sale.objects.first().id, self.sale.id)


    def test_update_sales_is_forbidden_if_user_is_not_authenticated(self):
        url = reverse('sale-detail', kwargs = { 'pk': 1 })

        data = {
            'amount_items_sold': 2,
            'sale_agent': 1,
            'product': 1
        }
        self.response = self.client.put(url, data, format='json')
        self.assertEqual(self.response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(Sale.objects.count(), 0)

    def test_update_sales_updates_object_if_user_is_authenticated(self):
        
        self.token, self.user = create_temporary_user()
        self.sale = create_temporary_sale(self.user)
        url = reverse('sale-detail', kwargs = { 'pk': self.sale.id })

        data = {
            'amount_items_sold': 2,
            'sale_agent': self.sale.sale_agent.id,
            'product': self.sale.product.id
        }

        self.client.credentials(HTTP_AUTHORIZATION='Token ' +self.token.key)
        self.response = self.client.put(url, data, format='json')
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)
        self.assertEqual(Sale.objects.count(), 1)
        self.assertEqual(Sale.objects.first().amount_items_sold, 2)


    def test_destroy_sales_is_forbidden_if_user_is_not_authenticated(self):
        url = reverse('sale-detail', kwargs = { 'pk': 1 })

        self.response = self.client.delete(url, format='json')
        self.assertEqual(self.response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(Sale.objects.count(), 0)

    def test_destroy_sales_destroys_object_if_user_is_authenticated(self):
        
        self.token, self.user = create_temporary_user()
        self.sale = create_temporary_sale(self.user)
        url = reverse('sale-detail', kwargs = { 'pk': self.sale.id })

        self.client.credentials(HTTP_AUTHORIZATION='Token ' +self.token.key)
        self.response = self.client.delete(url, format='json')
        self.assertEqual(self.response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Sale.objects.count(), 0)
