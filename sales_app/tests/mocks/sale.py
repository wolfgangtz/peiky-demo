from sales_app.models.sale import Sale

AMOUNT_ITEMS_SOLD_MOCK = 1

def mock_sale(product, sale_agent):

    sale = Sale(
        product=product,
        amount_items_sold=AMOUNT_ITEMS_SOLD_MOCK,
        sale_agent=sale_agent
    )
    return sale