from django.contrib import admin
from sales_app.models.sale import Sale


class SaleAdmin(admin.ModelAdmin):
    list_display = ('id', 'product', 'sale_date')
    search_fields = ('product__name', )
    list_per_page = 25


admin.site.register(Sale, SaleAdmin)
