from django.db import models
from simple_history.models import HistoricalRecords
from django.core.validators import MinValueValidator
from django.contrib.auth import get_user_model
from shops_app.models.product import Product


class Sale(models.Model):
    history = HistoricalRecords()
    User = get_user_model()
    product = models.ForeignKey(
        Product,
        on_delete=models.PROTECT,
        related_name='sales',
        null=False,
        blank=False
    )
    amount_items_sold = models.IntegerField(
        default=0,
        validators=[
            MinValueValidator(1)
        ]
    )
    sale_date = models.DateField(
        auto_now_add=True
    )

    sale_agent = models.ForeignKey(
        User,
        on_delete=models.PROTECT,
        null=False,
        blank=False
    )


    def __str__(self):
        return "Product Sold: '" + self.product.name + \
                "' In Shop:  '" + self.product.shop.name + "'"